library(raster, rgdal)

rikola <- brick("rikola_16bands_geocorrected_geotiff.tiff")
rikola_800 <- rikola$rikola_16bands_geocorrected_geotiff.15
rikola_710 <- rikola$rikola_16bands_geocorrected_geotiff.9

idv_CIre <- (rikola_800/rikola_710)-1

#writeRaster(idv_CIre, "idv_CIre.tif")