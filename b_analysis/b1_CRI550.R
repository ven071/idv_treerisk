library(raster)

rikola <- brick("rikola_16bands_geocorrected_geotiff.tiff")
rikola_515 <- rikola$rikola_16bands_geocorrected_geotiff.1
rikola_550 <- rikola$rikola_16bands_geocorrected_geotiff.3

b1_CRI550 <- (1/rikola_515)-(1/rikola_550)

#writeRaster(b1_CRI550, "b1_CRI550.tif")
